<?php

namespace Nitra\ExtensionsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Nitra\ExtensionsBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass;
use Nitra\ExtensionsBundle\DependencyInjection\Compiler\FormTemplateCompilerPass;

class NitraExtensionsBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideServiceCompilerPass());
        $container->addCompilerPass(new FormTemplateCompilerPass());
    }
}