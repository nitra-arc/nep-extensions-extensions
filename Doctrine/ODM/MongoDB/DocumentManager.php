<?php

namespace Nitra\ExtensionsBundle\Doctrine\ODM\Mongodb;

use Doctrine\ODM\MongoDB\DocumentManager as BaseDocumnetManager;
use Symfony\Component\DependencyInjection\Container;

class DocumentManager extends BaseDocumnetManager
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    /** @var \AppKernel */
    protected $kernel;

    public function setContainer(Container $container)
    {
        $this->container    = $container;
        $this->kernel       = $container->get('kernel');
    }

    /**
     * Create a new Query instance for a class.
     *
     * @param string $documentName The document class name.
     * @return Query\Builder
     */
    public function createQueryBuilder($documentName = null)
    {
        $documentName = $this->extendsQueryBuilder($documentName);
        return parent::createQueryBuilder($documentName);
    }

    /**
     * Gets the repository for a document class.
     *
     * @param string $documentName  The name of the Document.
     * @return DocumentRepository  The repository.
     */
    public function getRepository($documentName)
    {
        $documentName = $this->extendsQueryBuilder($documentName);
        return parent::getRepository($documentName);
    }

    /**
     * Расширение менеджера документов доктрины, для автоматического получения переопределенных документов 
     * @param string $documentName
     * @return string
     */
    protected function extendsQueryBuilder($documentName)
    {
        if ($documentName) {
            // Получаем разделитель в зависимости вида строки
            $delimeter          = stristr($documentName, ':') ? ':' : "\\";
            // разбиваем строку
            $documentBundle     = explode($delimeter, ltrim($documentName, '\\'));

            // получаем название бандла
            $sourceBundleName   = (count($documentBundle) > 2)
                ? ($documentBundle[0] . $documentBundle[1])
                : $documentBundle[0];
            if (key_exists($sourceBundleName, $this->kernel->getBundles())) {
                // получаем переопределенный бандл
                $bundle             = $this->kernel->getBundle($sourceBundleName);
                $bundleName         = $bundle->getName();
                $bundleNamespace    = $bundle->getNamespace();

                // Название класса
                $class              = end($documentBundle);
                // Существует класс?
                if (class_exists('\\' . $bundleNamespace . '\\Document\\' . $class)) {
                    // новое название документа
                    $documentName   = $bundleName . ':' . $class;
                }
            }

            return $documentName;
        }
    }
}