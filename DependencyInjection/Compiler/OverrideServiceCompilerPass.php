<?php

namespace Nitra\ExtensionsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $container->setParameter('doctrine_mongodb.odm.document_manager.class', 'Nitra\ExtensionsBundle\Doctrine\ODM\MongoDB\DocumentManager');
        $container->getDefinition('doctrine_mongodb.odm.default_document_manager')
            ->addMethodCall('setContainer', array(
                new Reference('service_container')
            ));
    }
}