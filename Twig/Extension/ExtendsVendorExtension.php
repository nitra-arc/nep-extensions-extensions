<?php

namespace  Nitra\ExtensionsBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Nitra\ExtensionsBundle\Twig\TokenParser\ExtendsVendorTokenParser;

class ExtendsVendorExtension extends \Twig_Extension
{
    protected $loader;

    public function __construct(\Twig_Loader_Filesystem $loader)
    {
        $this->loader = $loader;
    }

    public function addPath(ContainerInterface $container)
    {
        $this->loader->addPath($container->getParameter('kernel.root_dir').'/../vendor/');
    }

    public function getTokenParsers()
    {
        return array(
            new ExtendsVendorTokenParser(),
        );
    }

    public function getName()
    {
        return 'extends_vendor';
    }
}