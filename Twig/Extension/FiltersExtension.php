<?php

namespace Nitra\ExtensionsBundle\Twig\Extension;

/**
 * @inheritdoc
 */
class FiltersExtension extends \Twig_Extension
{
    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('methods', array($this, 'methods')),
            new \Twig_SimpleFilter('json_decode', 'json_decode'),
        );
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'e-commerce-extensions:filters-extension';
    }

    /**
     * get class methods allowed in template
     *
     * @param mixed $obj
     *
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    public function methods($obj)
    {
        $toReturn = array();
        switch (gettype ($obj)) {
            case 'array':
                $toReturn = array_keys($obj);
                break;
            case 'object':
                $methods = get_class_methods($obj);
                foreach ($methods as $method) {
                    if (preg_match('/^(is|get)[a-z]+/i', $method)) {
                        $toReturn[] = lcfirst(preg_replace('/^(is|get)/', '', $method));
                    }
                }
                break;
            default:
                throw new \InvalidArgumentException('Argument type must be one of object, array');
                break;
        }

        return $toReturn;
    }
}