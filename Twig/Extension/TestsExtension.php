<?php

namespace Nitra\ExtensionsBundle\Twig\Extension;

class TestsExtension extends \Twig_Extension
{
    public function getTests()
    {
        return array(
            new \Twig_SimpleTest('string',  'is_string'),
            new \Twig_SimpleTest('bool',    'is_bool'),
            new \Twig_SimpleTest('float',   'is_float'),
            new \Twig_SimpleTest('int',     'is_integer'),
            new \Twig_SimpleTest('object',  'is_object'),
        );
    }

    public function getName()
    {
        return 'e-commerce-extensions:tests-extension';
    }
}