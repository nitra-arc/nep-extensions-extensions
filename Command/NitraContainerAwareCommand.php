<?php

namespace Nitra\ExtensionsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;

abstract class NitraContainerAwareCommand extends ContainerAwareCommand
{
    const STYLE_NONE        = '0';
    const STYLE_BOLD        = '1';
    const STYLE_ITALIC      = '3';
    const STYLE_UNDERLINE   = '4';
    
    const COLOR_BLACK       = '0';
    const COLOR_RED         = '1';
    const COLOR_GREEN       = '2';
    const COLOR_YELLOW      = '3';
    const COLOR_BLUE        = '4';
    const COLOR_MAGENTA     = '5';
    const COLOR_CYAN        = '6';
    const COLOR_WHITE       = '7';
    
    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager()     { return $this->getContainer()->get('doctrine.odm.mongodb.document_manager'); }
    
    /** @return \Symfony\Component\Console\Helper\ProgressHelper */
    protected function getProgressHelper()      { return $this->getHelper('progress'); }
    /** @return \Symfony\Component\Console\Helper\TableHelper */
    protected function getTableHelper()         { return $this->getHelper('table'); }

    /**
     * @return \Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper
     */
    protected function getDialogHelper()
    {
        $dialog = $this->getHelperSet()->get('dialog');
        if (!$dialog || get_class($dialog) !== 'Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper') {
            $this->getHelperSet()->set($dialog = new DialogHelper());
        }

        return $dialog;
    }

    /**
     * @param string $str
     * @param string|array $style
     * @param string $color
     * @return string
     */
    protected function format($str, $style = self::STYLE_NONE, $fontColor = null, $backColor = null)
    {
        if (is_array($style)) {
            $style = implode(';', $style);
        }
        $colors = array();
        if (!is_null($fontColor)) {
            $colors[] = '3' . $fontColor;
        }
        if (!is_null($backColor)) {
            $colors[] = '4' . $backColor;
        }
        $format = $style . ($colors ? ';' : null) . implode(';' , $colors);
        
        return "\033[{$format}m{$str}\033[0m";
    }

    /**
     * @param int $size
     * @return string
     */
    protected function niceSize($size, $round = 4)
    {
        return $this->niceX($size, array('b', 'Kb', 'Mb', 'Gb', 'Tb'), 1024, $round);
    }
    
    /**
     * @param int $sec
     * @return string
     */
    protected function niceTime($sec, $round = 4)
    {
        return $this->niceX($sec, array('sec.', 'min.', 'hours.', 'days.', 'years.'), array(1000, 60, 60, 24, 355), $round);
    }
    
    /**
     * @param int $val
     * @param array $iec
     * @param int|array $ir
     * @param int $round
     * @return string
     */
    protected function niceX($val, $iec, $ir, $round = 4)
    {
        if (!is_array($ir)) {
            $ir = array_fill(0, count($iec), $ir);
        }
        $i = 0;
        while (($val / $ir[$i]) > 1) {
            $val = $val / $ir[$i];
            $i++;
        }
        return round($val, $round) . ' ' . $iec[$i];
    }
}