<?php

namespace Nitra\ExtensionsBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Sensio\Bundle\GeneratorBundle\Manipulator\KernelManipulator;

class OverrideBundleCommand extends NitraContainerAwareCommand
{
    /** @var \AppKernel */
    protected $kernel;
    /** @var \Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine */
    protected $twig;
    /** @var \Symfony\Component\HttpKernel\Bundle\BundleInterface */
    protected $bundle;
    /** @var \Symfony\Component\Console\Input\InputInterface */
    protected $input;
    /** @var \Symfony\Component\Console\Output\OutputInterface */
    protected $output;

    protected $allowDirs = array();
    protected $LastDir;
    protected $alias;
    protected $nameOfBundle;
    protected $currentDir;

    protected function configure()
    {
        $this->setName("nitra:overridebundle")
            ->setDescription("Override bundle")
            ->addArgument('name', InputArgument::OPTIONAL, 'bundle name')
            ->addArgument('prefix', InputArgument::OPTIONAL, 'new bundle name prefix');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->kernel = $this->getContainer()->get('kernel');
        $this->twig   = $this->getContainer()->get('templating');
        $this->output = $output;
        $this->input  = $input;
        $this->checkInput($input, $output);
        $this->config();
    }

    /**
     * проверка вводимых данных
     */
    protected function checkInput()
    {
        // получаем объект для работы с диалогами
        $dialog = $this->getDialogHelper();

        // если имя бандла не указано
        if (!$this->input->getArgument('name')) {
            // справшиваем, желает ли пользователь увидеть список всех зарегестрированных бандлов
            $show   = $dialog->askConfirmation($this->output, $dialog->getQuestion('Вы не ввели имя бандла. Желаете ли Вы просмотреть все установленные бандлы', 'yes', '?'));
            // если да, выводим
            if ($show) {
                $this->output->writeln(array_keys($this->kernel->getBundles()));
            }
            // просим пользователя ввести имя переопредеяемого бандла
            $name   = $dialog->ask($this->output, $dialog->getQuestion('Введите имя переопределяемого бандла', null, ':'));
            $this->_exit($name);
            $this->input->setArgument('name', $name);
        }

        // получаем бандл
        $this->bundle = $this->kernel->getBundle($this->input->getArgument('name'));
        // если бандл переопределен
        if ($this->bundle->getParent() == $this->input->getArgument('name')) {
            $this->output->writeln("Бандл {$this->format($this->input->getArgument('name'), self::STYLE_BOLD, self::COLOR_RED)} уже переопределен");
            $this->_exit();
        }

        // если не указан префикс нового бандла
        if ($this->input->getArgument('name') && !$this->input->getArgument('prefix')) {
            $prefix = $dialog->ask($this->output, $dialog->getQuestion('Введите приставку для переопределяемого бандла', null, ':'));
            $this->_exit($prefix);
            $this->input->setArgument('prefix', $prefix);
        }
    }

    /**
     * Метод для прекращения работы команды
     * @param string|null $arg
     */
    protected function _exit($arg = null)
    {
        if (!$arg) {
            exit();
        }
    }

    /**
     * Фун-ия определения переменных и массиво
     * @param \Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper $dialog
     * @param string $path
     */
    protected function config()
    {
        $dialog = $this->getDialogHelper();
        if ($dialog->askConfirmation($this->output, $dialog->getQuestion('Желаете ли Вы переопределить все контроллеры', 'yes', '?'))) {
            $this->allowDirs[] = 'Controller';
        }
        if ($dialog->askConfirmation($this->output, $dialog->getQuestion('Желаете ли Вы переопределить все шаблоны', 'yes', '?'))) {
            $this->allowDirs[] = 'Resources%dm%views';
        }
        $dirs   = preg_split('/vendor/', $this->bundle->getPath());
        $str    = preg_split('/' . mb_strtolower($this->nameOfBundle) . '/', $dirs[1]);
        $last               = substr($this->bundle->getPath(), strrpos($this->bundle->getPath(), '/') + 1);
        $this->alias        = $this->input->getArgument('prefix');
        $this->nameOfBundle = $last;
        $this->currentDir   = substr($str[0], 1, -1);
        $pathTo             = $this->getContainer()->get("kernel")->getRootDir() . '/../src/Nitra/' . $this->alias . $this->nameOfBundle;

        if ($this->nameOfBundle) {
            $this->startPoint($last, $pathTo, $this->bundle->getPath());
        }
    }

    /**
     * отправная точка, вызов остальных методов
     * @param string $last
     * @param string $pathTo
     */
    protected function startPoint($last, $pathTo)
    {
        $firstPart  = substr($this->input->getArgument('name'), 0, strrpos($this->input->getArgument('name'), $last));
        $namespace  = $firstPart . '\\' . $this->alias . $this->nameOfBundle;
        $bundleN    = $firstPart . $this->alias . $this->nameOfBundle;
        $this->mainClass($pathTo, $this->bundle->getPath());
        $this->updateKernel($namespace, $bundleN);
    }

    /**
     * создание корневой папоки и главного класса
     * @param string $pathTo
     * @param string $path
     */
    protected function mainClass($pathTo, $path)
    {
        if (!file_exists("$pathTo/")) {
            mkdir($pathTo, 0755, true);
        }
        $pathNameMainClass = 'src/Nitra/' . $this->alias . $this->nameOfBundle . DIRECTORY_SEPARATOR . 'Nitra' . $this->alias . $this->nameOfBundle;
        $file = realpath($this->getContainer()->get("kernel")->getRootDir() . '/../') . '/' . $pathNameMainClass . '.php';
        if (!file_exists($file)) {
            $content = $this->twig->render("NitraExtensionsBundle::bundle.php.twig", array(
                'alias'         => $this->alias,
                'nameBundle'    => $this->nameOfBundle,
                'parentBundle'  => $this->input->getArgument('name'),
            ));
            $current = fopen($file, "w+") or die('Файл не создан');
            fwrite($current, $content);
            fclose($current);
            if ($this->output->getVerbosity() > OutputInterface::VERBOSITY_NORMAL) {
                $this->output->writeln("Создали класс {$this->format("Nitra\\{$this->alias}{$this->nameOfBundle}\Nitra\\{$this->alias}{$this->nameOfBundle}", self::STYLE_BOLD, self::COLOR_RED)}");
            }
        } elseif ($this->output->getVerbosity() > OutputInterface::VERBOSITY_VERBOSE) {
            $this->output->writeln("Класс {$this->format("Nitra\\{$this->alias}{$this->nameOfBundle}\Nitra\\{$this->alias}{$this->nameOfBundle}", self::STYLE_BOLD, self::COLOR_RED)} уже создан");
        }
        $this->scanDir($path, $pathTo);
    }

    /**
     * сканируем подпапки и заносим их в массив, создаем файлы
     * @param string $pathFrom
     * @param string $pathTo
     */
    protected function owerride($pathFrom, $pathTo)
    {
        (array) $files = scandir($pathFrom);
        $subDirs = array();
        foreach ($files as $dir) {
            if (in_array($dir, array('.', '..'))) {
                continue;
            } elseif (is_dir($pathFrom . DIRECTORY_SEPARATOR . $dir)) {
                $subDirs[] = $dir;
            } else {
                $this->createFile($pathFrom, $pathTo, $dir);
            }
        }
        $this->subDirectory($subDirs, $pathTo, $pathFrom);
    }

    /**
     * создание подпапок
     * @param array $subDirs
     * @param string $pathTo
     * @param string $pathFrom
     */
    protected function subDirectory(array $subDirs, $pathTo, $pathFrom)
    {
        foreach ($subDirs as $dir) {
            $pathFromSrc = explode(DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR, $pathTo)[1] . DIRECTORY_SEPARATOR . $dir;
            if (!file_exists("$pathTo" . DIRECTORY_SEPARATOR . "$dir/")) {
                mkdir($pathTo . DIRECTORY_SEPARATOR . $dir, 0755, true);
                if ($this->output->getVerbosity() > OutputInterface::VERBOSITY_NORMAL) {
                    $this->output->writeln("\t\tСоздана папка {$this->format($pathFromSrc, self::STYLE_BOLD, self::COLOR_RED)}");
                }
            } elseif ($this->output->getVerbosity() > OutputInterface::VERBOSITY_VERBOSE) {
                $this->output->writeln("\t\tПапка уже создана {$this->format($pathFromSrc, self::STYLE_BOLD, self::COLOR_RED)}");
            }
            $this->LastDir = $dir;
            $this->owerride($pathFrom . DIRECTORY_SEPARATOR . $dir, $pathTo . DIRECTORY_SEPARATOR . $dir, $this->nameOfBundle, $this->alias, $this->output);
        }
    }

    /**
     * определени какой файл, php или twig, и в соответствии вызываем фун-ию
     * @param string $pathFrom
     * @param string $pathTo
     * @param string $dir
     */
    protected function createFile($pathFrom, $pathTo, $dir)
    {
        //если файл, узнаем его расширение и  имя
        $filePath = $pathTo . DIRECTORY_SEPARATOR . $dir;
        list($fileName, $fileFormat) = preg_split('/[.]/', $dir);
        if (!file_exists($filePath)) {
            switch ($fileFormat) {
                case 'php':
                    $this->createPhpFile($filePath, $fileName, $fileFormat);
                    break;
                case 'html':
                    $this->createTwigFile($filePath, $fileName, $fileFormat, $pathFrom);
                    break;
            }
        } elseif ($this->output->getVerbosity() > OutputInterface::VERBOSITY_VERBOSE) {
            $this->output->writeln("\t\t\tФайл {$this->format($fileName . '.' . $fileFormat, self::STYLE_BOLD, self::COLOR_RED)} " . str_repeat(' ', 60 - strlen($fileName . '.' . $fileFormat)) . "уже существует");
        }
    }

    /**
     * создание файла php
     * @param string $filePath
     * @param string $fileName
     * @param string $fileFormat
     */
    protected function createPhpFile($filePath, $fileName, $fileFormat)
    {
        $content = $this->twig->render("NitraExtensionsBundle::controller.php.twig", array(
            'alias'         => $this->alias,
            'nameBundle'    => $this->nameOfBundle,
            'lastDir'       => $this->LastDir,
            'fileName'      => $fileName,
        ));
        $currentFile = fopen($filePath, "w") or die("Файл не создан");

        fwrite($currentFile, $content);
        fclose($currentFile);
        if ($this->output->getVerbosity() > OutputInterface::VERBOSITY_NORMAL) {
            $this->output->writeln("\t\t\tПереопределили класс {$this->format($fileName . '.' . $fileFormat, self::STYLE_BOLD, self::COLOR_RED)}");
        }
    }

    /**
     * создание файла twig
     * @param string $filePath
     * @param string $fileName
     * @param string $fileFormat
     * @param string $pathFrom
     */
    protected function createTwigFile($filePath, $fileName, $fileFormat, $pathFrom)
    {
        $replacer = array(
            '/.*vendor\//'          => '',
            '/Resources\/views\//'  => '',
            '/\//'                  => ':',
        );
        $extend = preg_replace(array_keys($replacer), array_values($replacer), $pathFrom);
        $content = $this->twig->render("NitraExtensionsBundle::template.twig.twig", array(
            'extend'        => $extend,
            'fileName'      => $fileName,
            'fileFormat'    => $fileFormat,
        ));
        $currentFile = fopen($filePath, "w") or die("Файл не создан");
        fwrite($currentFile, $content);
        fclose($currentFile);
        if ($this->output->getVerbosity() > OutputInterface::VERBOSITY_NORMAL) {
            $this->output->writeln("\t\t\tПереопределили шаблон {$this->format($fileName . '.' . $fileFormat . '.twig', self::STYLE_BOLD, self::COLOR_RED)}");
        }
    }

    /**
     * обновление AppKernel
     * @param string $namespace
     * @param string $bundle
     */
    protected function updateKernel($namespace, $bundle)
    {
        $dialog = $this->getDialogHelper();
        $auto = true;
        if ($this->input->isInteractive()) {
            $auto = $dialog->askConfirmation($this->output, $dialog->getQuestion('Настроить AppKernel автоматически', 'yes', '?'), true);
        }
        $manip = new KernelManipulator($this->getContainer()->get('kernel'));
        if ($auto) {
            $manip->addBundle($namespace . '\\' . $bundle);
        }
    }

    /**
     * дописываем выбраные папки и создаем в src
     * @param string $pathFrom
     * @param string $pathTo
     */
    private function scanDir($pathFrom, $pathTo)
    {
        foreach ($this->allowDirs as $dir) {
            $path = DIRECTORY_SEPARATOR . str_replace('%dm%', DIRECTORY_SEPARATOR, $dir);
            if (is_dir($pathFrom . $path)) {
                if ($this->output->getVerbosity() > OutputInterface::VERBOSITY_NORMAL) {
                    $this->output->writeln("\tСканирование " . $this->format(trim($path, '/'), self::STYLE_BOLD, self::COLOR_RED));
                }
                if (!file_exists("{$pathTo}{$path}/")) {
                    mkdir($pathTo . $path, 0755, true);
                }
                $this->owerride($pathFrom . $path, $pathTo . $path);
            } elseif ($this->output->getVerbosity() > OutputInterface::VERBOSITY_VERBOSE) {
                $this->output->writeln("\tПапки {$this->format(trim($path, '/'), self::STYLE_BOLD, self::COLOR_RED)} не найдено в данном бандле");
            }
            $this->LastDir = null;
        }
    }
}